---
title: "Biografia"
date: 2021-09-18T16:59:34-03:00
draft: false
---

Nasci no começo da década de 2000, numa pequena cidade. No qual, estudei até o ensino médio. Após passar no vestibular da Universidade de São Paulo, mudei de cidade para poder estudar na USP, onde curso engenharia mecatrônica.

A jornada acadêmica, se mostrou umais difícil do que imaginava, além das matérias que exigiam um conhecimento que não tive contanto durante o ensino médio, havia também o desafio de se adaptar a uma nova cidade. Apesar das dificuldades, consigui me adaptar bem a nova fase da vida.

Com uma rotina de estudo estabelecida, decidi entrar em uma iniciação científica 
e também a prestar o processo seletivo para um grupo de extensão, com objetivo de aplicar os conhecimentos adquiridos em aula na prática.