---
title: "Maria Iolanda da Silva Feitosa"
date: 2021-09-18T16:59:34-03:00
draft: false
---

### Contato
Telefone: (11) 91111-111

Email: mariafeitosa@usp.br

### Educação
#### 2022 - atualmente      Graduação em engenharia mecatrônica
 * Universidade de São Paulo, USP, Brasil
### Experiência
#### 2023 - atualmente  Iniciação científica
 * Desenvolvimento de programa em Pyhton para resolver problemas de escoamento de fluido
 *  Universidade de São Paulo, USP, Brasil
### Competências e habilidades
 * Conhecimento médio em Python;
 * Conhcecimento básico em HTML, C++;
 * Nível básico em espanhol
 * Capacidade de comunicação
 * Trabalho em equipe