---
title: "Dinâmica dos fluidos computacional"
date: 2021-09-18T23:28:40-03:00
draft: false
---

Nessa iniciação científica, o objetivo é desenvolver programas usando Python para obter soluções de problemas de dinâmica dos fluidos.

Comecei a iniciação em 2023, o primeiro ano foi dedicado a adquirir a base teórica, como características de equações diferencias parcias, aproximações por diferenças finitas. Além desses tópicos, também havia o foco de estudar a dinâmica dos fluidos. Assim nessa etapa, estudei sobre escoamento do fluido, equação de convecção, equação de difusão e também as equações de Navier-Stokes, para problemas unidimensional e bidimensional.

No ano de 2024, começou a etapa de programar. Inicialmente, os problemas serviam para aprender a utilizar a biblioteca Numpy do Python e também a como utilizar arrays, para resolver problemas bidimensionais. A etapa seguinte, a qual estou atualmente, é utilizar a programação para calcular a velocidade e a pressão de um fluido que percorre um tubo, no qual pode haver algum obstáculo, dentro dele.