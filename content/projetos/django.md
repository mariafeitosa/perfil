---
title: "Equipe Poli de Baja"
date: 2021-09-18T23:27:57-03:00
draft: false
---

A equipe é dividida em subsistema, como suspensão e direção, powertrain, freios, planejamento, elétrico e análise estrutural. Participo dos subsistemas de planejamento e de suspensão e direção.

Como membro do planejamento, entro em contanto com os patrocinadores, ajudo a planejar algumas atividades, como
aula de metrologia para os novos membros. Também participo, de eventos organizado por outras instituição da Universidade,
por exemplo, o Meninas na Poli e na semana de recepção dos bixos no começo do ano, no qual apresentamos sobre a equipe para as outras pessoas.

Já como membro da suspensão e direção, ajudo a fabricar as peças referentes a essa área e a montar esse subsistema no carro.


